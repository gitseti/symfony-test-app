<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class SessionController extends Controller
{
    /**
     * @Route("/login")
     */
    public function login_user()
    {
        return $this->render('session/login.html.twig');
    }
}
