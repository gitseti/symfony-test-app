<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class TestController extends Controller
{
    /**
     * @Route("/test/wastl")
     */
    public function show_wastl()
    {
        return $this->render('test/wastl.html.twig');
    }

    /**
     * @Route("/test/jan")
     */
    public function show_jan()
    {
        return $this->render('test/jan.html.twig');
    }
}
