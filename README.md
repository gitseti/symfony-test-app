
<h1>Folgende Befehle müssen ausgeführt werden, um die App lokal zum Laufen zu bringen: </h1>
 <ol>
<li>$ git clone https://gitlab.com/gitseti/symfony-test-app.git</li>
<li>$ cd symfony-test-app</li>
<li>$ curl -sS https://getcomposer.org/installer | php </li>
<li>$ php composer.phar install </li>
<li>$ php bin/console server:run</li>
 <li>Im Browser <a href="http://localhost:8000">http://localhost:8000</a> aufrufen</li>
 </ol>

 <h1>So mitarbeiten: </h1>
<ol>
<li>Öffne den symfony-test-app Ordner mit einem Skripteditor wie atom oder sublime</li>
<li>Unter app/Resources/views findest du die Ordner mit den html-Seiten</li>
<li>Die CSS-Dateien sollten in web/css hinzugefügt werden und werden über href="{{ asset('css/bmeinecssdatei.css') }} im html Code referenziert</li>
<li>Um die Navigationsbar auf deiner Seite einzubinden füge als Erstes {% extends 'base.html.twig' %} in deine html Datei ein</li>
<li>Füge dein HTML-Code in so ein: <br>
<code>{% block body %} Dein Html Code {% endblock %}</code></li>
<li>Nutze <code>$ php bin/console server:run</code> um deine Seite über <a href="http://localhost:8000/yourroute">http://localhost:8000</a> aufzurufen</li>
<li>Bist du zufrieden mit dem Ergebnis nutze git um einen Pull request zu schicken</li>
<li>Hierrauf update ich die Heroku App und alle können auf die neuste Version zugreifen</li>
</ol>
